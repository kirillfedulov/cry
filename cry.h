#include <string.h>

#ifdef CRYALL

#define CRYMD5
#define CRYSHA1

#endif  // CRYALL

#ifdef CRYMD5

struct md5Context_t {
    unsigned char b[64];
    unsigned int h[4];
    unsigned long n;
};

void md5Init  (md5Context_t *ctx);
void md5Update(md5Context_t *ctx, const unsigned char *msg, unsigned long msglen);
void md5Final (md5Context_t *ctx, unsigned char *digest);

#endif  // CRYMD5

#ifdef CRYSHA1

struct sha1Context_t {
    unsigned char b[64];
    unsigned int h[5];
    unsigned long n;
};

void sha1Init  (sha1Context_t *ctx);
void sha1Update(sha1Context_t *ctx, const unsigned char *msg, unsigned long msglen);
void sha1Final (sha1Context_t *ctx, unsigned char *digest);

#endif  // CRYSHA1

#ifdef CRYIMPL

static inline unsigned int circularRotate(unsigned int x, unsigned int n) {
    return (x << n) | (x >> (32 - n));
}

#ifdef CRYMD5

static void md5Step(unsigned int *h, const unsigned char *m) {
    unsigned int a = h[0], b = h[1], c = h[2], d = h[3], x[16];

    for (unsigned long i = 0, k = 0; i < 64; i += 4, k++) {
        x[k] = (m[i+3] << 24) | (m[i+2] << 16) | (m[i+1] << 8) | m[i];
    }

    a = b + circularRotate(a + ((b & c) | (~b & d)) + x[ 0] + 0xd76aa478u, 7);
    d = a + circularRotate(d + ((a & b) | (~a & c)) + x[ 1] + 0xe8c7b756u, 12);
    c = d + circularRotate(c + ((d & a) | (~d & b)) + x[ 2] + 0x242070dbu, 17);
    b = c + circularRotate(b + ((c & d) | (~c & a)) + x[ 3] + 0xc1bdceeeu, 22);
    a = b + circularRotate(a + ((b & c) | (~b & d)) + x[ 4] + 0xf57c0fafu, 7);
    d = a + circularRotate(d + ((a & b) | (~a & c)) + x[ 5] + 0x4787c62au, 12);
    c = d + circularRotate(c + ((d & a) | (~d & b)) + x[ 6] + 0xa8304613u, 17);
    b = c + circularRotate(b + ((c & d) | (~c & a)) + x[ 7] + 0xfd469501u, 22);
    a = b + circularRotate(a + ((b & c) | (~b & d)) + x[ 8] + 0x698098d8u, 7);
    d = a + circularRotate(d + ((a & b) | (~a & c)) + x[ 9] + 0x8b44f7afu, 12);
    c = d + circularRotate(c + ((d & a) | (~d & b)) + x[10] + 0xffff5bb1u, 17);
    b = c + circularRotate(b + ((c & d) | (~c & a)) + x[11] + 0x895cd7beu, 22);
    a = b + circularRotate(a + ((b & c) | (~b & d)) + x[12] + 0x6b901122u, 7);
    d = a + circularRotate(d + ((a & b) | (~a & c)) + x[13] + 0xfd987193u, 12);
    c = d + circularRotate(c + ((d & a) | (~d & b)) + x[14] + 0xa679438eu, 17);
    b = c + circularRotate(b + ((c & d) | (~c & a)) + x[15] + 0x49b40821u, 22);

    a = b + circularRotate(a + ((b & d) | (c & ~d)) + x[ 1] + 0xf61e2562u, 5);
    d = a + circularRotate(d + ((a & c) | (b & ~c)) + x[ 6] + 0xc040b340u, 9);
    c = d + circularRotate(c + ((d & b) | (a & ~b)) + x[11] + 0x265e5a51u, 14);
    b = c + circularRotate(b + ((c & a) | (d & ~a)) + x[ 0] + 0xe9b6c7aau, 20);
    a = b + circularRotate(a + ((b & d) | (c & ~d)) + x[ 5] + 0xd62f105du, 5);
    d = a + circularRotate(d + ((a & c) | (b & ~c)) + x[10] + 0x2441453u, 9);
    c = d + circularRotate(c + ((d & b) | (a & ~b)) + x[15] + 0xd8a1e681u, 14);
    b = c + circularRotate(b + ((c & a) | (d & ~a)) + x[ 4] + 0xe7d3fbc8u, 20);
    a = b + circularRotate(a + ((b & d) | (c & ~d)) + x[ 9] + 0x21e1cde6u, 5);
    d = a + circularRotate(d + ((a & c) | (b & ~c)) + x[14] + 0xc33707d6u, 9);
    c = d + circularRotate(c + ((d & b) | (a & ~b)) + x[ 3] + 0xf4d50d87u, 14);
    b = c + circularRotate(b + ((c & a) | (d & ~a)) + x[ 8] + 0x455a14edu, 20);
    a = b + circularRotate(a + ((b & d) | (c & ~d)) + x[13] + 0xa9e3e905u, 5);
    d = a + circularRotate(d + ((a & c) | (b & ~c)) + x[ 2] + 0xfcefa3f8u, 9);
    c = d + circularRotate(c + ((d & b) | (a & ~b)) + x[ 7] + 0x676f02d9u, 14);
    b = c + circularRotate(b + ((c & a) | (d & ~a)) + x[12] + 0x8d2a4c8au, 20);

    a = b + circularRotate(a + (b ^ c ^ d) + x[ 5] + 0xfffa3942u, 4);
    d = a + circularRotate(d + (a ^ b ^ c) + x[ 8] + 0x8771f681u, 11);
    c = d + circularRotate(c + (d ^ a ^ b) + x[11] + 0x6d9d6122u, 16);
    b = c + circularRotate(b + (c ^ d ^ a) + x[14] + 0xfde5380cu, 23);
    a = b + circularRotate(a + (b ^ c ^ d) + x[ 1] + 0xa4beea44u, 4);
    d = a + circularRotate(d + (a ^ b ^ c) + x[ 4] + 0x4bdecfa9u, 11);
    c = d + circularRotate(c + (d ^ a ^ b) + x[ 7] + 0xf6bb4b60u, 16);
    b = c + circularRotate(b + (c ^ d ^ a) + x[10] + 0xbebfbc70u, 23);
    a = b + circularRotate(a + (b ^ c ^ d) + x[13] + 0x289b7ec6u, 4);
    d = a + circularRotate(d + (a ^ b ^ c) + x[ 0] + 0xeaa127fau, 11);
    c = d + circularRotate(c + (d ^ a ^ b) + x[ 3] + 0xd4ef3085u, 16);
    b = c + circularRotate(b + (c ^ d ^ a) + x[ 6] + 0x4881d05u, 23);
    a = b + circularRotate(a + (b ^ c ^ d) + x[ 9] + 0xd9d4d039u, 4);
    d = a + circularRotate(d + (a ^ b ^ c) + x[12] + 0xe6db99e5u, 11);
    c = d + circularRotate(c + (d ^ a ^ b) + x[15] + 0x1fa27cf8u, 16);
    b = c + circularRotate(b + (c ^ d ^ a) + x[ 2] + 0xc4ac5665u, 23);

    a = b + circularRotate(a + (c ^ (b | ~d)) + x[ 0] + 0xf4292244u, 6);
    d = a + circularRotate(d + (b ^ (a | ~c)) + x[ 7] + 0x432aff97u, 10);
    c = d + circularRotate(c + (a ^ (d | ~b)) + x[14] + 0xab9423a7u, 15);
    b = c + circularRotate(b + (d ^ (c | ~a)) + x[ 5] + 0xfc93a039u, 21);
    a = b + circularRotate(a + (c ^ (b | ~d)) + x[12] + 0x655b59c3u, 6);
    d = a + circularRotate(d + (b ^ (a | ~c)) + x[ 3] + 0x8f0ccc92u, 10);
    c = d + circularRotate(c + (a ^ (d | ~b)) + x[10] + 0xffeff47du, 15);
    b = c + circularRotate(b + (d ^ (c | ~a)) + x[ 1] + 0x85845dd1u, 21);
    a = b + circularRotate(a + (c ^ (b | ~d)) + x[ 8] + 0x6fa87e4fu, 6);
    d = a + circularRotate(d + (b ^ (a | ~c)) + x[15] + 0xfe2ce6e0u, 10);
    c = d + circularRotate(c + (a ^ (d | ~b)) + x[ 6] + 0xa3014314u, 15);
    b = c + circularRotate(b + (d ^ (c | ~a)) + x[13] + 0x4e0811a1u, 21);
    a = b + circularRotate(a + (c ^ (b | ~d)) + x[ 4] + 0xf7537e82u, 6);
    d = a + circularRotate(d + (b ^ (a | ~c)) + x[11] + 0xbd3af235u, 10);
    c = d + circularRotate(c + (a ^ (d | ~b)) + x[ 2] + 0x2ad7d2bbu, 15);
    b = c + circularRotate(b + (d ^ (c | ~a)) + x[ 9] + 0xeb86d391u, 21);

    h[0] += a;
    h[1] += b;
    h[2] += c;
    h[3] += d;
}

void md5Init(md5Context_t *ctx) {
    memset(ctx, 0, sizeof(*ctx));

    ctx->h[0] = 0x67452301;
    ctx->h[1] = 0xefcdab89;
    ctx->h[2] = 0x98badcfe;
    ctx->h[3] = 0x10325476;
}

void md5Update(md5Context_t *ctx, const unsigned char *msg, unsigned long msglen) {
    unsigned long nbuffered = ctx->n%64,
                  nleft     = 64-nbuffered,
                  i         = 0;
    if (msglen >= nleft) {
        memcpy(ctx->b+nbuffered, msg, nleft);
        md5Step(ctx->h, ctx->b);

        for (i = nleft; i+63 < msglen; i += 64)
            md5Step(ctx->h, msg+i);

        nbuffered = 0;
    }

    ctx->n += msglen;
    memcpy(ctx->b+nbuffered, msg+i, msglen-i);
}

void md5Final(md5Context_t *ctx, unsigned char *digest) {
    static const unsigned char padding[64] = { 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    unsigned long nbuffered = ctx->n%64,
                  npadding  = nbuffered < 56 ? 56-nbuffered : 120-nbuffered,
                  nbits     = 8*ctx->n;
    md5Update(ctx, padding, npadding);
    
    unsigned char bits[8] = {};
    for (int i = 0; i < 8; i++)
        bits[i] = (unsigned char) ((nbits >> (8*i)) & 0xff);
    md5Update(ctx, bits, 8);

    for (int i = 0; i < 4; i++) {
        digest[4*i  ] = (unsigned char) (ctx->h[i]         & 0xff);
        digest[4*i+1] = (unsigned char) ((ctx->h[i] >>  8) & 0xff);
        digest[4*i+2] = (unsigned char) ((ctx->h[i] >> 16) & 0xff);
        digest[4*i+3] = (unsigned char) ((ctx->h[i] >> 24) & 0xff);
    }
}

#endif  // CRYMD5

#ifdef CRYSHA1

static void sha1Step(unsigned int *h, const unsigned char *x) {
    unsigned int a = h[0], b = h[1], c = h[2], d = h[3], e = h[4], w[80];

    for (int i = 0, k = 0; i < 64; i += 4, k++)
        w[k] = (x[i] << 24) | (x[i+1] << 16) | (x[i+2] << 8) | x[i+3];

    for (int i = 16; i < 80; i++)
        w[i] = circularRotate(w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16], 1);

    for (int i = 0; i < 80; i++) {
        unsigned int f = 0, k = 0;
        switch (i/20) {
            case 0:
                f = (b & c) | (~b & d);
                k = 0x5a827999U;
                break;
            case 1:
                f = b ^ c ^ d;
                k = 0x6ed9eba1U;
                break;
            case 2:
                f = (b & c) | (b & d) | (c & d);
                k = 0x8f1bbcdcU;
                break;
            case 3:
                f = b ^ c ^ d;
                k = 0xca62c1d6U;
                break;
        }

        unsigned int temp = circularRotate(a, 5) + f + e + w[i] + k;
        e = d;
        d = c;
        c = circularRotate(b, 30);
        b = a;
        a = temp;
    }

    h[0] += a;
    h[1] += b;
    h[2] += c;
    h[3] += d;
    h[4] += e;
}

void sha1Init(sha1Context_t *ctx) {
    memset(ctx, 0, sizeof(*ctx));

    ctx->h[0] = 0x67452301;
    ctx->h[1] = 0xefcdab89U;
    ctx->h[2] = 0x98badcfe;
    ctx->h[3] = 0x10325476;
    ctx->h[4] = 0xc3d2e1f0;
}

void sha1Update(sha1Context_t *ctx, const unsigned char *msg, unsigned long msglen) {
    unsigned long nbuffered = ctx->n%64,
                  nleft     = 64-nbuffered,
                  i         = 0;
    if (msglen >= nleft) {
        memcpy(ctx->b+nbuffered, msg, nleft);
        sha1Step(ctx->h, ctx->b);

        for (i = nleft; i+63 < msglen; i += 64)
            sha1Step(ctx->h, msg+i);

        nbuffered = 0;
    }

    ctx->n += msglen;
    memcpy(ctx->b+nbuffered, msg+i, msglen-i);
}

void sha1Final(sha1Context_t *ctx, unsigned char *digest) {
    static const unsigned char padding[64] = { 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    unsigned long nbuffered = ctx->n%64,
                  npadding  = nbuffered < 56 ? 56-nbuffered : 120-nbuffered,
                  nbits     = 8*ctx->n;
    sha1Update(ctx, padding, npadding);
    
    unsigned char bits[8] = {};
    for (int i = 0; i < 8; i++)
        bits[i] = (unsigned char) ((nbits >> (56-8*i)) & 0xff);
    sha1Update(ctx, bits, 8);

    for (int i = 0; i < 5; i++) {
        digest[4*i  ] = (unsigned char) ((ctx->h[i] >> 24) & 0xff);
        digest[4*i+1] = (unsigned char) ((ctx->h[i] >> 16) & 0xff);
        digest[4*i+2] = (unsigned char) ((ctx->h[i] >>  8) & 0xff);
        digest[4*i+3] = (unsigned char) (ctx->h[i]         & 0xff);
    }
}

#endif  // CRYSHA1

#endif  // CRYIMPL
