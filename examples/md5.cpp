#include <stdio.h>

#define CRYMD5
#define CRYIMPL
#include "../cry.h"

int main(int argc, char *argv[]) {
    unsigned char b[2048] = {};
    size_t nread = 0;

    md5Context_t ctx = {};
    md5Init(&ctx);
    while ((nread = fread(b, 1, sizeof(b), stdin)) > 0)
        md5Update(&ctx, b, nread);
    unsigned char digest[16] = {};
    md5Final(&ctx, digest);

    for (int i = 0; i < 16; i++) {
        printf("%02x", digest[i]);
    }
    printf("\n");
}
