#include <stdio.h>

#define CRYSHA1
#define CRYIMPL
#include "../cry.h"

int main(int argc, char *argv[]) {
    unsigned char b[2048] = {};
    size_t nread = 0;

    sha1Context_t ctx = {};
    sha1Init(&ctx);
    while ((nread = fread(b, 1, sizeof(b), stdin)) > 0)
        sha1Update(&ctx, b, nread);
    unsigned char digest[20] = {};
    sha1Final(&ctx, digest);

    for (int i = 0; i < 20; i++)
        printf("%02x", digest[i]);
    printf("\n");
}
