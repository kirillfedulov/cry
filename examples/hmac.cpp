#include <stdio.h>

#define CRYALL
#define CRYIMPL
#include "../cry.h"

void hmac(const unsigned char *msg, unsigned long msglen, const unsigned char *key, unsigned long keylen, unsigned char *mac) {
    unsigned char b[64] = {};
    if (keylen > 64) {
        sha1Context_t ctx = {};
        sha1Init(&ctx);
        sha1Update(&ctx, key, keylen);
        sha1Final(&ctx, b);
    } else
        memcpy(b, key, keylen);

    for (int i = 0; i < 64; i++)
        b[i] ^= 0x36;

    sha1Context_t ctx = {};
    sha1Init(&ctx);
    sha1Update(&ctx, b, 64);
    sha1Update(&ctx, msg, msglen);
    sha1Final(&ctx, mac);

    for (int i = 0; i < 64; i++)
        b[i] = b[i] ^ 0x36 ^ 0x5c;

    sha1Init(&ctx);
    sha1Update(&ctx, b, 64);
    sha1Update(&ctx, mac, 20);
    sha1Final(&ctx, mac);
}

int main(int argc, char *argv[]) {
    unsigned char mac[20] = {};
    hmac((const unsigned char *) "private data", strlen("private data"),
         (const unsigned char *) "hello", strlen("hello"), mac);

    for (int i = 0; i < 20; i++)
        printf("%02x", mac[i]);
    printf("\n");
}
